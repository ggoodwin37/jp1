Each batch is just the Documents folder from an app dump, renamed to a unique name.

// xcode 7
to take a dump:
  window -> devices -> choose device on left -> select the install app -> gear menu -> download -> save it to ~/tmp or whatever

to replace a dump:
  same, except upload the image instead

make sure app isn't running on device (even in background)
make sure device is passcode unlocked just in case

documents folder is here:
com.goodguyapps.JumpProto 2015-08-19 21:13.31.406.xcappdata/AppData/Documents
